﻿using Shapes.Figures;
using System;


namespace Shapes
{
    public interface Shape : Drawable
    {
        float StartX();

        float StartY();

        float Width();

        float Height();

    }
}
