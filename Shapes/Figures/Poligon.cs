﻿using System.Drawing;

namespace Shapes.Figures
{
    public class Poligon : Drawable
    {
        protected readonly PointF[] pts;
        protected readonly Color c;
        protected readonly bool filled;

        public Poligon(PointF[] points, Color color, bool fill = false) 
        {
            c = color;
            pts = points;
            filled = fill;
        }

        public virtual void Draw(Graphics graphics)
        {
            if (filled)
            {
                Brush brush = new SolidBrush(c);
                graphics.FillPolygon(brush, pts);
                brush.Dispose();
            }
            else
            {
                Pen pen = new Pen(c);
                graphics.DrawPolygon(pen, pts);
                pen.Dispose();
            }
        }
    }
}
