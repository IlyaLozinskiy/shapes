﻿using System.Drawing;

namespace Shapes.Figures
{
    public class Ellipse : Rect
    {

        public Ellipse(float startX, float startY, float width, float height, Color color)
           : base(startX, startY, width, height, color)
        {

        }

        public override void Draw(Graphics graphics)
        {
            Pen pen = new Pen(c);
            graphics.DrawEllipse(pen, GetDescribingRectangle());
            pen.Dispose();
        }
    }
}
