﻿using System.Drawing;

namespace Shapes.Figures.Lines
{
    public class Line : Shape
    {

        protected readonly float x1;
        protected readonly float y1;
        protected readonly float x2;
        protected readonly float y2;
        protected readonly float w;
        protected readonly Color c;

        public Line(float startX, float startY, float endX, float endY, Color color, float tickness = 1)
        {
            x1 = startX;
            y1 = startY;
            x2 = endX;
            y2 = endY;
            c = color;
            w = tickness;
        }

        public float StartX()
        {
            return x1;
        }

        public float StartY()
        {
            return y1;
        }

        public float EndX()
        {
            return x2;
        }

        public float EndY()
        {
            return y2;
        }

        public float Width()
        {
            return w;
        }
        public float Height()
        {
            return 1;
        }

        public virtual void Draw(Graphics graphics)
        {
            Pen pen = new Pen(c, w);
            graphics.DrawLine(pen, x1, y1, x2, y2);
            pen.Dispose();
        }

        protected Rectangle GetDescribingRectangle()
        {
            return new Rectangle((int)x1, (int)y1, (int)(x2 - x1), (int)(y2 - y1));
        }
    }
}
