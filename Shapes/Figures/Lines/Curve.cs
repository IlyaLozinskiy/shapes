﻿using System.Drawing;

namespace Shapes.Figures.Lines
{
    public class Curve : Line
    {
        protected readonly PointF[] pts;

        public Curve(PointF[] points, Color color, float tickness = 1) : base(points[0].X, points[0].Y, points[points.Length - 1].X, points[points.Length - 1].Y, color, tickness)
        {
            pts = points;
        }

        public override void Draw(Graphics graphics)
        {
            Pen pen = new Pen(c, w);
            graphics.DrawCurve(pen, pts);
            pen.Dispose();
        }

    }
}
