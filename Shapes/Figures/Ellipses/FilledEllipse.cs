﻿using System.Drawing;

namespace Shapes.Figures.Ellipses
{
    public class FilledEllipse : Ellipse
    {
        public FilledEllipse(float startX, float startY, float width, float height, Color fillColor)
            : base(startX, startY, width, height, fillColor)
        {

        }

        public override void Draw(Graphics graphics)
        {
            Brush brush = new SolidBrush(c);
            graphics.FillEllipse(brush, GetDescribingRectangle());
            brush.Dispose();
        }
    }
}
