﻿
using System.Drawing;

namespace Shapes.Figures.Ellipses
{
    public class Circle : Ellipse
    {

        public Circle(float centerX, float centerY, float radius, Color color) : base(centerX - radius, centerY - radius, radius * 2, radius * 2, color)
        {

        }
    }
}
