﻿using System.Drawing;

namespace Shapes.Figures.Ellipses
{
    class FilledCircle : FilledEllipse
    {

        public FilledCircle(float centerX, float centerY, float radius, Color fillColor) : base(centerX - radius, centerY - radius, radius * 2, radius * 2, fillColor)
        {

        }
    }
}
