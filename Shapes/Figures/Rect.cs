﻿using System.Drawing;

namespace Shapes.Figures
{
    public class Rect : Shape
    {
        protected readonly float x;
        protected readonly float y;
        protected readonly float w;
        protected readonly float h;
        protected readonly Color c;

        public Rect(float startX, float startY, float width, float height, Color color)
        {
            x = startX;
            y = startY;
            w = width;
            h = height;
            c = color;
        }

        public float StartX()
        {
            return x;
        }

        public float StartY()
        {
            return y;
        }

        public float Width()
        {
            return w;
        }
        public float Height()
        {
            return h;
        }

        public virtual void Draw(Graphics graphics)
        {
            Pen pen = new Pen(c);
            graphics.DrawRectangle(pen, GetDescribingRectangle());
            pen.Dispose();
        }

        protected Rectangle GetDescribingRectangle()
        {
            return new Rectangle((int)x, (int)y, (int)w, (int)h);
        }
    }
}
