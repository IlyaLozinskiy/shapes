﻿using System.Drawing;

namespace Shapes.Figures
{
    public class FilledRectangle : Rect
    {

        public FilledRectangle(float startX, float startY, float width, float height, Color fillColor)
            : base(startX, startY, width, height, fillColor)
        {

        }

        public override void Draw(Graphics graphics)
        {
            Brush brush = new SolidBrush(c);
            graphics.FillRectangle(brush, GetDescribingRectangle());
            brush.Dispose();
        }
    }
}
