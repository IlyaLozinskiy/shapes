﻿using System;
using System.Drawing;

namespace Shapes.Figures
{
    public class GradientRectangle : FilledRectangle
    {

        public GradientRectangle(float startX, float startY, float width, float height, Color startColor)
            : base(startX, startY, width, height, startColor)
        {

        }

        public override void Draw(Graphics graphics)
        {
            int alpha = 255;
            int yEnd = (int)(y + h);
            Pen pen = new Pen(Color.FromArgb(alpha, c));
            for (int j = (int)y; j < yEnd; j++, alpha -= 1 + (int)Math.Floor(255 / h))
            {
                graphics.DrawLine(pen, x, y + j, x + w, y + j);
                pen.Color = Color.FromArgb(Math.Max(alpha, 1), c);
            }
            pen.Dispose();
        }
    }
}
