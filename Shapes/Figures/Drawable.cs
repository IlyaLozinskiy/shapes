﻿using System.Drawing;

namespace Shapes.Figures
{
   public interface Drawable
    {
         void Draw(Graphics graphics);
    }
}
