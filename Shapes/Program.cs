﻿using Shapes.Figures;
using Shapes.Figures.Ellipses;
using Shapes.Figures.Lines;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Shapes
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Scene scene = new SceneBuilder()
              .AddDrawable(new Rect(20, 30, 200, 300, Color.White))
              .AddDrawable(new FilledRectangle(210, 310, 50, 50, Color.Bisque))
              .AddDrawable(new GradientRectangle(100, 100, 200, 110, Color.LightCyan))
              .AddDrawable(new Ellipse(23, 1.4f, 50, 333, Color.Beige))
              .AddDrawable(new FilledEllipse(239, 11.4f, 52, 32, Color.Firebrick))
              .AddDrawable(new Circle(100, 100, 50, Color.Purple))
              .AddDrawable(new FilledCircle(400, 400, 100, Color.Purple))
              .AddDrawable(new Line(320, 320, 390, 353, Color.Orange))
              .AddDrawable(new Curve(new PointF[]
              {
                    new PointF(50.0F, 510.0F),
                    new PointF(150.0F, 315.0F),
                    new PointF(50.0F, 151.0F),
                    new PointF(140.0F, 510.0F),
                    new PointF(35.0F, 210.0F),
                    new PointF(30.0F, 401.0F),
                    new PointF(150.0F, 254.0F),
              }, Color.RosyBrown, 3))
              .AddDrawable(new Poligon(new PointF[]
              {
                    new PointF(50.0F, 50.0F),
                    new PointF(100.0F, 25.0F),
                    new PointF(200.0F, 5.0F),
                    new PointF(250.0F, 50.0F),
                    new PointF(300.0F, 100.0F),
                    new PointF(350.0F, 200.0F),
                    new PointF(250.0F, 250.0F),
              }, Color.BlueViolet))
              .AddDrawable(new Poligon(new PointF[]
              {
                    new PointF(100.0F, 150.0F),
                    new PointF(200.0F, 125.0F),
                    new PointF(300.0F, 15.0F),
                    new PointF(450.0F, 150.0F),
                    new PointF(400.0F, 10.0F),
                    new PointF(450.0F, 20.0F),
                    new PointF(450.0F, 20.0F),
              }, Color.ForestGreen, true))
              .Build();
            Application.Run(scene);
        }
    }
}
