﻿
namespace Shapes
{
    partial class Scene
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Scene
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1000, 1000);
            this.Name = "Scene";
            this.Text = "Scene";
            this.Load += new System.EventHandler(this.Scene_Load);
            this.ResumeLayout(false);

        }

        #endregion
    }
}

