﻿using Shapes.Figures;
using System.Collections.Generic;

namespace Shapes
{
    public class SceneBuilder
    {
        protected readonly List<Drawable> drawables;

        public SceneBuilder()
        {
            drawables = new List<Drawable>();
        }

        public SceneBuilder AddDrawable(Drawable drawable)
        {
            drawables.Add(drawable);
            return this;
        }

        public Scene Build()
        {
            return new Scene(drawables);
        }

    }
}
