﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using Shapes.Figures;

namespace Shapes
{
    public partial class Scene : Form
    {

        private readonly Panel canvas;
        private readonly List<Drawable> drawables;

        public Scene() : this(null) { }

        public Scene(List<Drawable> drawables)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;
            canvas = new Panel();
            this.drawables = drawables;
        }

        private void Scene_Load(object sender, EventArgs e)
        {
            canvas.Dock = DockStyle.Fill;
            canvas.BackColor = Color.Black;
            canvas.Paint += new PaintEventHandler(DrawItems);
            Controls.Add(canvas);
        }

        private void DrawItems(object sender, PaintEventArgs e)
        {
            drawables.ForEach(d => d.Draw(e.Graphics));
        }

    }
}
